#include "CrackPacket.h"

static char tmp[1024];

void CrackPacket::StorePacket(PacketHeader* hPacket, char* side)
{
	FILE* fs;

	sprintf(tmp, "Plugin/Packet/%s/%Xh - %db.bin", side, hPacket->PacketId, hPacket->Size);

	fopen_s(&fs, tmp, "wb");

	if(fs == NULL)
	{
		sprintf(tmp, "Can't store packet 0x%X", hPacket->PacketId);
		MessageBox(NULL, tmp, "Error : fopen", MB_OK);
	}
	else
	{
		fwrite((void*)hPacket, sizeof(BYTE), hPacket->Size, fs);

		fclose(fs);
	}

	printf("%s => Opcode: %X   Size: %d   ClientId: %d\n", side, hPacket->PacketId, hPacket->Size, hPacket->ClientId);
}

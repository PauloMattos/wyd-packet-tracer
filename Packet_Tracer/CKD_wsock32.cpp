// ---
// by ptr0x
// ---

#include "CKD_wsock32.h"

static BYTE recv_bufr[65536];
static BYTE send_bufr[655360];

int CKD_wsock32::FKD_recv(SOCKET s, char *bufr, int len, int flags)
{
	int rt = recv(s, bufr, len, flags);

	memcpy(recv_bufr, bufr, *(WORD*)bufr);

	WYD::DecryptPacket(recv_bufr);

	CrackPacket::StorePacket((PacketHeader*)recv_bufr, "Recv");

	return rt;
}

int CKD_wsock32::FKD_send(SOCKET s, char *bufr, int len, int flags)
{
	int sr = send(s, bufr, len, flags);

	// Hello packet
	if(!(*(DWORD*)bufr == 521270033 && *(WORD*)&bufr[4] == 2))
	{
		memcpy(send_bufr, bufr, *(WORD*)bufr);

		WYD::DecryptPacket((BYTE*)send_bufr);

		CrackPacket::StorePacket((PacketHeader*)send_bufr, "Send");
	}

	return sr;
}
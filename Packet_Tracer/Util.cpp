#include "Util.h"

static char tmp[1024];

void Util::LogError(char *errorName, char *errorDesc)
{
	FILE *fs;

	sprintf(tmp, "LogError/%s.txt", errorName);

	fopen_s(&fs, tmp, "wt");

	if(fs != NULL)
	{
		fputs(errorDesc, fs);

		fclose(fs);
	}
}
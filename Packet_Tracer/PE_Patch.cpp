#include "PE_Patch.h"

int jmp_addr;

void PE_Patch::DoIndirections()
{
	PE_Patch::wsock32_recv();
	PE_Patch::wsock32_send();
}

void PE_Patch::wsock32_recv()
{
	// IAT Hook
	DWORD dw = PE_Patch::PatchIAT(GetModuleHandle(NULL), (PROC)GetProcAddress(GetModuleHandle("wsock32.dll"), "recv"), (PROC)CKD_wsock32::FKD_recv);

	if(!dw)
	{
		MessageBox(NULL, "O patch na IAT n�o p�de ser completado.", "Patching error : recv", NULL);
		exit(1);
	}
}

void PE_Patch::wsock32_send()
{
	// IAT Hook
	DWORD dw = PE_Patch::PatchIAT(GetModuleHandle(NULL), (PROC)GetProcAddress(GetModuleHandle("wsock32.dll"), "send"), (PROC)CKD_wsock32::FKD_send);

	if(!dw)
	{
		MessageBox(NULL, "O patch na IAT n�o p�de ser completado.", "Patching error : send", NULL);
		exit(1);
	}
}

DWORD PE_Patch::PatchIAT(HMODULE hMod, PROC origFunc, PROC newFunc)
{
	PIMAGE_DOS_HEADER pDosH;
	PIMAGE_NT_HEADERS pNTH;
	PIMAGE_IMPORT_DESCRIPTOR pImportDesc;
	PIMAGE_EXPORT_DIRECTORY pExportDir;
	PIMAGE_THUNK_DATA pThunk;
	PIMAGE_IMPORT_BY_NAME pImportName;

	if(!newFunc || !hMod || !origFunc)
		return 0;

	// Verify that the newFunc is valid
	if (IsBadCodePtr(newFunc))
		return 0;

	// Get DOS Header
	pDosH = (PIMAGE_DOS_HEADER) hMod;

	// Verify that the PE is valid by checking e_magic's value and DOS Header size
	if(IsBadReadPtr(pDosH, sizeof(IMAGE_DOS_HEADER)))
		return 0;

	if(pDosH->e_magic != IMAGE_DOS_SIGNATURE)
		return 0;

	// Find the NT Header by using the offset of e_lfanew value from hMod
	pNTH = (PIMAGE_NT_HEADERS) ((DWORD) pDosH + (DWORD) pDosH->e_lfanew);

	// Verify that the NT Header is correct
	if(IsBadReadPtr(pNTH, sizeof(IMAGE_NT_HEADERS)))
		return 0;

	if(pNTH->Signature != IMAGE_NT_SIGNATURE)
		return 0;

	// iat patching
	pImportDesc = (PIMAGE_IMPORT_DESCRIPTOR) ((DWORD) pDosH +
	(DWORD) (pNTH->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress));

	if(pImportDesc == (PIMAGE_IMPORT_DESCRIPTOR) pNTH)
		return 0;

	while(pImportDesc->Name)
	{
		// pImportDesc->Name gives the name of the module, so we can find "user32.dll"
		char *name = (char *) ((DWORD) pDosH + (DWORD) (pImportDesc->Name));
		// stricmp returns 0 if strings are equal, case insensitive
		if(_stricmp(name, "wsock32.dll") == 0)
		{
			pThunk = (PIMAGE_THUNK_DATA)((DWORD) pDosH + (DWORD) pImportDesc->FirstThunk);
			while(pThunk->u1.Function)
			{
				// get the pointer of the imported function and see if it matches up with the original
				if((DWORD) pThunk->u1.Function == (DWORD) origFunc)
				{
					MEMORY_BASIC_INFORMATION mbi;
					DWORD oldProt;
					VirtualQuery(&pThunk->u1.Function, &mbi, sizeof(MEMORY_BASIC_INFORMATION));
					VirtualProtect(mbi.BaseAddress, mbi.RegionSize, PAGE_READWRITE, &oldProt);
					pThunk->u1.Function = (DWORD) newFunc;
					VirtualProtect(mbi.BaseAddress, mbi.RegionSize, oldProt, &oldProt);

					return 1;
				}
				else
				{
					++pThunk;
				} 
			}
		}

		++pImportDesc;
	}

	return 0; 
}
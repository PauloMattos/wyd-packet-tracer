// ---
// by ptr0x
// ---

#ifndef __CRACK_PACKET_H__
#define __CRACK_PACKET_H__

#include <Windows.h>
#include <stdio.h>

#include "WYD.h"

class CrackPacket
{
public:
	static void StorePacket(PacketHeader* hPacket, char* side);
};

#endif // __CRACK_PACKET_H__

// ---
// by ptr0x
// ---

#ifndef __PE_PATCH_H__
#define __PE_PATCH_H__

#include "CKD_wsock32.h"
#include "PE_Hook.h"

void NKD_ChatGM();
class PE_Patch
{
private:
	static DWORD PatchIAT(HMODULE hMod, PROC origFunc, PROC newFunc);
	static void wsock32_send();
	static void wsock32_recv();
	
public:
	static void DoIndirections();
};

#endif // __PE_PATCH_H__
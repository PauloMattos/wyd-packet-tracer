#include "WYD.h"
#include "PE_Patch.h"

void WYD::DecryptPacket(BYTE* buffer)
{
	PacketHeader* packet = (PacketHeader*)buffer;

	DWORD keyIncrement = keyTable[(buffer[2] * 2)];
    DWORD keyResult = 0;

	for (DWORD i = 4, thisIterator = 0; i < packet->Size;
            i++, keyIncrement++, thisIterator = i & 3)
    {
        keyResult = keyTable[((keyIncrement & 0x800000FF) * 2) + 1];

        if (thisIterator == 0)
        {
            buffer[i] -= (BYTE)(keyResult << 1);
        }
        else if (thisIterator == 1)
        {
            buffer[i] += (BYTE)((int)(keyResult) >> 3);
        }
        else if (thisIterator == 2)
        {
            buffer[i] -= (BYTE)(keyResult << 2);
        }
        else if (thisIterator == 3)
        {
            buffer[i] += (BYTE)((int)keyResult >> 5);
        }
    }
}
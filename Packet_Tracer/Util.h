// ---
// by ptr0x
// ---

#ifndef __UTIL_H__
#define __UTIL_H__

#include <Windows.h>
#include <stdio.h>

class Util
{
public:
	static void LogError(char *errorName, char *errorDesc);
};

#endif // __UTIL_H__
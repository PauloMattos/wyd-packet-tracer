// ---
// by ptr0x
// ---

#ifndef __READ_MESSAGE_H__
#define __SE__READ_MESSAGE_H__ND_H__

#include <Windows.h>
#include <WinSock.h>

#pragma comment (lib, "wsock32.lib")

#include "CrackPacket.h"
#include "WYD.h"

class CKD_wsock32
{
public:
	static int FKD_recv(SOCKET s, char *bufr, int len, int flags);
	static int FKD_send(SOCKET s, char *bufr, int len, int flags);
};

#endif // __READ_MESSAGE_H__